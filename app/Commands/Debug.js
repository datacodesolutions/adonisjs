'use strict'

const { Command } = require('@adonisjs/ace')

class Debug extends Command {
  static get signature () {
    return 'debug'
  }

  static get description () {
    return 'Debug'
  }

  async handle (args, options) {
    this.info('Debugging')

    process.exit()
  }
}

module.exports = Debug
