const { hooks } = require('@adonisjs/ignitor')

hooks.before.aceCommand(() => {
  console.log('Before Ace Command Hook')
})

hooks.after.providersBooted(() => {
  console.log('After Providers Booted Hook')
})